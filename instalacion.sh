#!/bin/bash

#Preparamos y actualizamos antes de comenzar a instalar
sudo apt update && sudo apt upgrade

sudo apt update
#Intalamos curl y dependencias necesarias
sudo apt install -y curl gnupg2 dirmngr git-core zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev

# Requerimos instaler Node.js
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

sudo apt install -y nodejs
# Requerimos instaler Yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

echo "Instalación de Ruby completada `ruby --version`"

#instalacion de apache
sudo apt -y install apache2

#ruby
sudo apt-get update
sudo apt-get install -y curl gnupg build-essential
sudo gpg2 --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
sudo curl -sSL https://get.rvm.io | sudo bash -s stable
sudo if sudo grep -q secure_path /etc/sudoers; then sudo sh -c "echo export rvmsudo_secure_path=1 >> /etc/profile.d/rvm_secure_path.sh" && echo Environment variable installed; fi
sudo rvm install ruby-2.7.0
sudo rvm --default use ruby-2.7.0

#instalacion de ruby on rails
sudo apt install gcc g++ make -y
gem update --system
echo "gem: --no-document" >> ~/.gemrc
sudo gem install rails -v 6.1.4

#passenger
sudo apt update
sudo apt -y install passenger
sudo apt install -y libapache2-mod-passenger

#userdir
a2enmod userdir
systemctl restart apache2
mkdir ~/public_html

#mysql
cd /tmp
wget https://dev.mysql.com/get/mysql-apt-config_0.8.22-1_all.deb
sudo dpkg -i mysql-apt-config*
sudo apt update && sudo apt install mysql-server -y
sudo apt-get install mysql-client libmysqlclient-dev
sudo gem install mysql2

#redis
exit
