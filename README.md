# README

Entrar a root con  
`$ su -`   
`$ apt install sudo -y`    
`$ usermod -aG sudo 'usuario'`    
`$ su - 'usuario'`   

Primero el pull de github en la siguiente carpeta /var/www/proyecto con los siguientes comandos  
`$ sudo mkdir -p /var/www/proyecto`  
`$ sudo cd /var/www/proyecto `  
`$ git clone https://gitlab.com/JeanPaulSR/proyecto-redes.git`  

Despues entra a la carpeta del proyecto  
`$ cd proyecto-redes`  
`$ sudo chmod +x instalacion.sh`  
`$ sh instalacion.sh`  
Cuando MySQL se instala, asegura escoger el version que quieres  

Después de la instalación, cambia la contraseña en el archivo proyecto/config/database.yml con la contraseña root.  

`$ sudo nano /etc/apache2/apache2.conf `  
Y agregar:  
LoadModule passenger_module /home/user/.rbenv/versions/2.7.0/lib/ruby/gems/passenger-6.0.12/buildout/apache2/mod_passenger.so  

Cambia ‘user’ por el nombre de usuario  
```
<IfModule mod_passenger.c>  
     PassengerRoot /home/user/.rbenv/versions/2.7.0/lib/ruby/gems/2.7.0/gems/$  
     PassengerDefaultRuby /home/user/.rbenv/versions/2.7.0/bin/ruby  
</IfModule>  
```
Ejecuta los siguientes lineas para activar el archivo de userdir  
`$ sudo chmod 711 /home/user`  
`$ sudo chmod 755 /home/user/public_html`  

Para ejecutar el servidor, usa el siguiente comando  
`$ rails s` 
