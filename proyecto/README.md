# README

Primero el pull de github en la siguiente carpeta /var/www/proyecto con los siguientes comandos  
`$ mkdir -p /var/www/proyecto`  

`$ cd /var/www/proyecto `  
Entrar a root con  
`$ su -`  
`$ git clone https://gitlab.com/JeanPaulSR/proyecto-redes.git`  

Despues entra a la carpeta del proyecto  
`$ cd proyecto-redes`  
`$ sudo chmod +x instalacion.sh`  
`$ sh instalacion.sh`  
Cuando MySQL se instala, asegura escoger el version que quieres  

Después de la instalación, cambia la contraseña en el archivo proyecto/config/database.yml con la contraseña root.  

`$ sudo nano /etc/apache2/apache2.conf `  
Y agregar:  
LoadModule passenger_module /home/user/.rbenv/versions/2.7.0/lib/ruby/gems/passenger-6.0.12/buildout/apache2/mod_passenger.so  

Cambia ‘user’ por el nombre de usuario  
<IfModule mod_passenger.c>  
     PassengerRoot /home/user/.rbenv/versions/2.7.0/lib/ruby/gems/2.7.0/gems/$  
     PassengerDefaultRuby /home/user/.rbenv/versions/2.7.0/bin/ruby  
</IfModule>  

Para ejecutar el servidor, usa el siguiente comando  
`$ rails s`  
